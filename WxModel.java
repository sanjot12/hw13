import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import javafx.scene.image.Image;

//import com.google.gson.*;    //not necessary

/**
 * Created by Tim McGowen on 3/27/2018
 * Edited by Sanjot Chandi
 * Model to get weather information based on zipcode.
 * NOTE: This code was designed to work with Weather Underground.
 *       This site is no longer available. Use the code as an
 *       example but it will not work directly with Open Weather.
 */
public class WxModel {
  private JsonElement jse;

  public boolean getWx(String zipCode)
  {
    try
    {
      URL wxURL = new URL("http://api.openweathermap.org/data/2.5/weather?zip="
					+ zipCode
					+ "&appid=9fe109706ecf99638405c23136d9fbaa&units=imperial");

      // Open connection
      InputStream is = wxURL.openStream();
      BufferedReader br = new BufferedReader(new InputStreamReader(is));

      // Read the results into a JSON Element
      jse = new JsonParser().parse(br);

      // Close connection
      is.close();
      br.close();
   }
   catch (java.io.UnsupportedEncodingException uee)
   {
     uee.printStackTrace();
   }
   catch (java.net.MalformedURLException mue)
   {
     mue.printStackTrace();
   }
   catch(java.io.FileNotFoundException fnf)
   {
      return false;
   }
   catch (java.io.IOException ioe)
   {
     ioe.printStackTrace();
   }
   catch (java.lang.NullPointerException npe)
   {
     npe.printStackTrace();
   }
    // Check to see if the zip code was valid.
   return isValid();
 }


  public String getLocation()
  {
    String city=jse.getAsJsonObject().get("name").getAsString();
    String state=jse.getAsJsonObject().get("sys").getAsJsonObject().get("country").getAsString();
    return city+", "+state ;//city and country
  }

  public String getTime()
  {
     int timeStamp=jse.getAsJsonObject().get("dt").getAsInt();
     java.util.Date time=new java.util.Date((long)timeStamp*1000);
     return time+"";
  }

  public String getWeather()
  {
    return jse.getAsJsonObject().get("weather").getAsJsonArray().get(0).getAsJsonObject().get("description").getAsString();
  }

  public double getTemperature()
  {
    return jse.getAsJsonObject().get("main").getAsJsonObject().get("temp").getAsDouble();
  }

  public String getWind()
  {
    return(jse.getAsJsonObject().get("wind").getAsJsonObject().get("speed").getAsString()+" "+windDirection());
  }

  public String getPressure()
  {
    float pressure =(Math.round(0.02953*(jse.getAsJsonObject().get("main").getAsJsonObject().get("pressure").getAsDouble())) * 1000) / 1000;
    return pressure+"";
  }

  public String getHumidity()
  {
    return ""+jse.getAsJsonObject().get("main").getAsJsonObject().get("humidity").getAsInt();
    //return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("visibility_mi").getAsString();
  }

  public Image getImage()
  {
    String iconURL = jse.getAsJsonObject().get("weather").getAsJsonArray().get(0).getAsJsonObject().get("icon").getAsString();
    String url="http://openweathermap.org/img/wn/"+iconURL+"@2x.png";
    return new Image(url);
  }
  public boolean isValid()
  {
    // If the zip is not valid we will get an error field in the JSON
    try {
      String error = jse.getAsJsonObject().get("message").getAsString();
      return false;
    }

    catch (java.lang.NullPointerException npe)
    {
      // We did not see error so this is a valid zip
      //System.out.println(jse.getAsJsonObject().get("message").getAsString());
      return true;
    }
   }
    //turn meteorological directions(degress) to compass direction(N, E, S,W)
  public String windDirection()
  {
    int value = jse.getAsJsonObject().get("wind").getAsJsonObject().get("speed").getAsInt();
    int directionValue=(value/45);
    int list[]={0,1,2,3,4,5,6,7};
    String direction[]={"E","NE","N","NW","W","SW","S","SE"};
    return(direction[list[directionValue]]);
  }
  

}